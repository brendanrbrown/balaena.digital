module App.Balaena where

import Prelude
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Elements as HES
import Halogen.HTML.Properties as HEP

type State = Unit

data Action = NoOp

component :: forall q i o m. H.Component q i o m
component =
  H.mkComponent
    { initialState: \_ -> unit
    , render: \_ -> renderImg
    , eval: H.mkEval H.defaultEval
    }

renderImg :: forall cs m. H.ComponentHTML Action cs m
renderImg = HH.div_
  [ HES.img
      [ HEP.src whaleImg
      ]
  ]

whaleImg :: String
whaleImg =  "https://us.whales.org/wp-content/uploads/sites/2/2018/07/bowhead-whale-alan-airey-sg-7-1024x705.jpg"
